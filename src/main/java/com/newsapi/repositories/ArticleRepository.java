package com.newsapi.repositories;

import com.newsapi.entities.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by pranas on 16.11.13.
 */
public interface ArticleRepository extends MongoRepository<Article,String> {
}
