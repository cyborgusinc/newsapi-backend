package com.newsapi.services;

import com.newsapi.entities.Article;
import com.newsapi.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by pranas on 16.11.13.
 */

@Service
public class ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    public void addArticles(List<Article> articles){
        articleRepository.save(articles);
    }
}
