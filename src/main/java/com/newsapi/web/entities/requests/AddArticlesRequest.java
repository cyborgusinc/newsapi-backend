package com.newsapi.web.entities.requests;

import com.newsapi.entities.Article;

import java.util.List;

/**
 * Created by pranas on 16.11.13.
 */
public class AddArticlesRequest {

    private String source;

    private List<ArticleRequestEntity> articles;

    public AddArticlesRequest(){}

    public AddArticlesRequest(String source, List<ArticleRequestEntity> articles) {
        this.source = source;
        this.articles = articles;
    }

    public List<ArticleRequestEntity> getArticles() {
        return articles;
    }

    public String getSource() {
        return source;
    }
}
