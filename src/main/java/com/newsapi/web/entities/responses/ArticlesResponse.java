package com.newsapi.web.entities.responses;

import java.io.Serializable;

/**
 * Created by pranas on 16.11.13.
 */
public class ArticlesResponse implements Response{
    private int count;
    private String source;

    public ArticlesResponse(){}

    public ArticlesResponse(int count, String source){
        this.count = count;
        this.source = source;
    }

    public int getCount() {
        return count;
    }

    public String getSource() {
        return source;
    }
}
