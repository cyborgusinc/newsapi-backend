package com.newsapi.web.controllers;

import com.newsapi.entities.Article;
import com.newsapi.services.ArticleService;
import com.newsapi.web.entities.requests.AddArticlesRequest;
import com.newsapi.web.entities.responses.ArticlesResponse;
import com.newsapi.web.entities.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/post_articles")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST)
    public Response post(@RequestBody AddArticlesRequest request) {
        articleService.addArticles(request.getArticles().stream().map(p -> new Article(p)).collect(Collectors.toList()));
        return new ArticlesResponse(request.getArticles().size(), request.getSource());
    }
}