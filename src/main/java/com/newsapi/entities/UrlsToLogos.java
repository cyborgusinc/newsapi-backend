package com.newsapi.entities;

/**
 * Created by pranas on 16.11.10.
 */
public class UrlsToLogos {

    private String small;
    private String medium;
    private String large;

    public UrlsToLogos(){}

    public UrlsToLogos(String small, String medium, String large) {
        this.small = small;
        this.medium = medium;
        this.large = large;
    }

    public String getSmall() {
        return small;
    }

    public String getMedium() {
        return medium;
    }

    public String getLarge() {
        return large;
    }
}
