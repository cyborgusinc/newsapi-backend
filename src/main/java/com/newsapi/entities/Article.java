package com.newsapi.entities;

import com.newsapi.web.entities.requests.ArticleRequestEntity;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pranas on 16.10.30.
 */
@Document
public class Article implements Serializable{
    @Id
    private String id;
    private String author;
    private String title;
    private String description;
    private String url;
    private String urlToImage;
    private Date publishedAt;

    public Article(){
        this.id = new ObjectId().toHexString();
    }

    public Article(ArticleRequestEntity entity){
        this.id = new ObjectId().toHexString();
        this.author = entity.getAuthor();
        this.title = entity.getTitle();
        this.description = entity.getDescription();
        this.url = entity.getUrl();
        this.urlToImage = entity.getUrlToImage();
        this.publishedAt = entity.getPublishedAt();
    }


    public Article(String author, String title, String description, String url, String urlToImage, Date publishedAt) {
        this.id = new ObjectId().toHexString();
        this.author = author;
        this.title = title;
        this.description = description;
        this.url = url;
        this.urlToImage = urlToImage;
        this.publishedAt = publishedAt;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public String getId() {
        return id;
    }
}
